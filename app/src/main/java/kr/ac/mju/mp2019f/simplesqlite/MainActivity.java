package kr.ac.mju.mp2019f.simplesqlite;

import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import static android.icu.text.MessagePattern.ArgType.SELECT;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onInsert(View view) {
        SQLiteDatabase db = openOrCreateDatabase("mydb", MODE_PRIVATE, null );
        db.execSQL( "CREATE TABLE IF NOT EXISTS students (sID INTEGER PRIMARY KEY AUTOINCREMENT," +
                "firstname TEXT, lastname TEXT)");
        db.execSQL( "INSERT INTO students VALUES (null, 'Minho', 'Shin') " );
        db.execSQL( "INSERT INTO students (firstname, lastname)VALUES ('Yoowon', 'Shin') " );
        db.close();
    }

    public void onSelect(View view) {
        //String[] params = {"1", "Minho"};
        SQLiteDatabase db = openOrCreateDatabase("mydb", MODE_PRIVATE, null );
        Cursor cursor = db.rawQuery("SELECT * FROM students", null);
        //Cursor cursor = db.rawQuery("SELECT * FROM students WHERE sID=? AND firstname = ?", params);
        cursor.moveToFirst();
        do {
            Toast.makeText(this,
                    cursor.getString( cursor.getColumnIndex("firstname") ) + " " +
                    cursor.getString( cursor.getColumnIndex( "lastname")),
                    Toast.LENGTH_LONG ).show();

        } while( cursor.moveToNext());

        db.close();
    }
}
